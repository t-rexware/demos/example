package com.guzman.universityplan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversityPlanApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniversityPlanApplication.class, args);
    }

}
