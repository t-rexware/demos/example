package com.guzman.universityplan.entity;

import com.guzman.universityplan.config.database.Extensions;
import com.guzman.universityplan.config.database.Tables;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
@Table(name= Tables.TABLE_UNIT)
public class Unit {
    @Id
    @GeneratedValue(generator=Extensions.Identifier.GENERATOR)
    @GenericGenerator(name=Extensions.Identifier.GENERATOR, strategy=Extensions.Identifier.STRATEGY)
    private String id;

    private String name;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name=Tables.FK_UNIVERSITY)
    private University university;
}
