package com.guzman.universityplan.entity;

import com.guzman.universityplan.config.database.Extensions;
import com.guzman.universityplan.config.database.Tables;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

/*
* Class that represents University Table
* @author Jhair Guzman
* */
@Data
@Entity
@Table(name= Tables.TABLE_UNIVERSITY)
public class University {
    @Id
    @GeneratedValue(generator= Extensions.Identifier.GENERATOR)
    @GenericGenerator(name=Extensions.Identifier.GENERATOR, strategy=Extensions.Identifier.STRATEGY)
    private String id;

    private String name;
    private String logo;

    @OneToMany(mappedBy=Tables.ATTRIBUTE_ON_UNIT_FOR_UNIVERSITY)
    private Set<Unit> units;
}
