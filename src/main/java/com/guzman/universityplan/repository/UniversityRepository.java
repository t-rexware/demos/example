package com.guzman.universityplan.repository;

import com.guzman.universityplan.entity.University;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/*
* Interface for database operations definition
* related with university
* */
public interface UniversityRepository extends JpaRepository<University, String> {
    List<University> findUniversitiesByNameIsLike(String nameSearch);
}
