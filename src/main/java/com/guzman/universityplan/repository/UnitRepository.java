package com.guzman.universityplan.repository;

import com.guzman.universityplan.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UnitRepository extends JpaRepository<Unit, String> {
    List<Unit> findAllByNameLike(String name);
}
