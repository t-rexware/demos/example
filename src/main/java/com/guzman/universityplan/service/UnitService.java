package com.guzman.universityplan.service;

import com.guzman.universityplan.entity.Unit;
import com.guzman.universityplan.repository.UnitRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* Class the defines the Unit Service using repositories
*/
@Transactional(readOnly=true)
public class UnitService {
    private final UnitRepository unitRepository;

    public UnitService(UnitRepository unitRepository) {
        this.unitRepository = unitRepository;
    }

    /**
     * Method that creates a unit and persist it
     * @param unit new unit without id
     * @return Unit object with id
     */
    @Transactional
    public Unit create(Unit unit) {
        return this.unitRepository.save(unit);
    }


    /**
     * Method that updates the unit passed in the persistence layer
     * @param unit unit with fields updated to save
     * @return Unit object updated
     */
    @Transactional
    public Unit update(Unit unit) {
        return this.unitRepository.save(unit);
    }

    /**
     * Method that deletes the given unit in the persistence layer
     * @param unit with id to delete
     */
    @Transactional
    public void delete(Unit unit) {
        this.unitRepository.delete(unit);
    }


    public List<Unit> getAllByUniversity(String universityName) {
        return this.unitRepository.findAllByNameLike(universityName);
    }
}
