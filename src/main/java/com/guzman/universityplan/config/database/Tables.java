package com.guzman.universityplan.config.database;

public class Tables {

    public static final String TABLE_UNIT = "MT_UNIT";
    public static final String TABLE_UNIVERSITY = "MT_UNIVERSITY";

    public static final String FK_UNIVERSITY = "FK_UNIVERSITY";

    public static final String ATTRIBUTE_ON_UNIT_FOR_UNIVERSITY = "university";
}
