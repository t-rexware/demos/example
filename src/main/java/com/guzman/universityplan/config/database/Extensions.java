package com.guzman.universityplan.config.database;

public class Extensions {
    public static final class Identifier {
        public static final String GENERATOR = "system-uuid";
        public static final String STRATEGY  = "uuid2";
    }
}
